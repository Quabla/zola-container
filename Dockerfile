FROM docker.io/voidlinux/voidlinux
RUN xbps-install -Sy zola bash python3
RUN xbps-install -Sy python3-numpy python3-matplotlib python3-scipy
ENTRYPOINT /usr/bin/bash
